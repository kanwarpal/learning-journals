from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'learning_logs'
urlpatterns = [
    #home page
    path('', views.index, name='index'),

    #List of TOPICS page.
    path('topics/', views.topics, name='topics'),
]